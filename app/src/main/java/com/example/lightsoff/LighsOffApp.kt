package com.example.lightsoff

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LightsOffApp() {

    val navController = rememberNavController()

Scaffold(
    modifier = Modifier.fillMaxSize()
) { paddingValues ->
    Column(Modifier.padding(paddingValues)) {
      NavigationCenter(navController)
    }
}
    
}