package com.example.lightsoff

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.lightsoff.ui.screens.game.GameScreen
import com.example.lightsoff.ui.screens.HomeScreen

sealed class Screens(val route: String, @StringRes val title: Int){
    object Home: Screens("home", R.string.homescreen_title)
    object Game: Screens("game", R.string.gamescreen_title)
    object Highscore: Screens("score", R.string.highscore_screen_title)
}

@Composable
fun NavigationCenter(
    navController: NavHostController
) {


    NavHost(navController = navController, startDestination = Screens.Home.route ) {

        composable(route = Screens.Home.route) {
            HomeScreen(
                goToGame = { navController.navigate(Screens.Game.route) },
                goToScore = { navController.navigate(Screens.Highscore.route)},
            )
        }

        composable(route = Screens.Game.route) {

            
                GameScreen()
        }

        composable(route = Screens.Highscore.route){

        }

    }
}