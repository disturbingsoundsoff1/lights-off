package com.example.lightsoff.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.lightsoff.ui.theme.LightsOffTheme

@Composable
fun HomeScreen(
    goToGame: () -> Unit,
    goToScore: () -> Unit,
) {
    Column (
        Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ){
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            Button(onClick = { goToGame() }) {
                Text(text = "Играть")
            }
            Button(onClick = { goToScore() }) {
                Text(text = "Рекорды")
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun HomeScreenPreview() {
    LightsOffTheme(darkTheme = true) {
        Surface {
            HomeScreen(
                {},
                {}
            )
        }
    }
}
