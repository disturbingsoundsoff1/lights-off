package com.example.lightsoff.ui.screens.game

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.lightsoff.ui.theme.LightsOffTheme


@OptIn(ExperimentalStdlibApi::class)
@Composable
fun GameScreen(
    list: List<Boolean>,
    handleClick: (Int, Int) -> Unit,
    shuffle: () -> Unit
) {

    Column(
        Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Column(
            Modifier.weight(2f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom,
        ) {
            for (i in 0..<5) {
                Row(
                ) {
                    for (j in 0..<5) {
                        Cell(
                            state = list[i*5+j],
                            x = i,
                            y = j,
                            handleClick = {x,y -> handleClick(x,y)}
                        )
                    }
                }
            }
        }


        Column(
            Modifier.weight(1f),
            verticalArrangement = Arrangement.Bottom
        ){
            Button(
                modifier = Modifier.padding(bottom = 55.dp),
                onClick = { shuffle() }
            ) {
                Text(text = "Перемешать", fontSize = 22.sp)
            }
        }
    }
}

@Composable
fun Cell(state: Boolean, x: Int, y: Int, handleClick: (Int, Int) -> Unit) {
    Box(
        modifier = Modifier
            .width(65.dp)
            .height(65.dp)
            .padding(2.dp)
            .clip(RoundedCornerShape(15))
            .background(
                if (state) Color.White
                else Color.Black
            )
            .border(1.dp, MaterialTheme.colorScheme.primary, RoundedCornerShape(15))
            .clickable {
                handleClick(x, y)
            }
    )
}


@Preview(showSystemUi = true)
@Composable
fun GameScreenPreview() {
    LightsOffTheme(darkTheme = true) {
        Surface {
            val list = mutableListOf<Boolean>()
            repeat(25){
               list.add(false)
            }
            GameScreen(
                list,
                {x,y ->},
                {}
            )
        }
    }
}